const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()

exports.main = async (event, context) => {
	const wxContext = cloud.getWXContext()
	let shop = await db.collection('shop').doc(event._id).get()
	console.log(shop, wxContext)
	if (!shop.data._openid || shop.data._openid != wxContext.OPENID) {
		return {
			code: 20000,
			msg: '申请记录不存在'
		}
	}
	await db.collection('shop').doc(event._id).update({
		data: {
			update_time: new Date(),
			status:0,
			examine: [{
				status: 0,
				time: new Date()
			}].concat(shop.data.examine)
		}
	})
	shop = await db.collection('shop').doc(event._id).get()
	console.log('更新后', shop.data)
	return {
		code: 200,
		msg:'取消成功',
		data: shop.data
	}
}