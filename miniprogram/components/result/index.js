Component({
	properties: {
		text: {
			type: String,
			value: '页面加载中...'
		},
		icon:{
			type:String,
			value:'info'
		},
		loading:{
			type:Boolean,
			value:false
		}
	},
	options: {
		addGlobalClass: true,
	},
	methods: {

	}
})
