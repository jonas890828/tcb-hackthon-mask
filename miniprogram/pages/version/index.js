const app = getApp()
Page({
	data: {
		page:1,
		list:[]
	},
	onLoad: function (options) {
		this.getData()
	},
	getData(page=1){
		app.db.collection('version').orderBy('create_time', 'desc').skip(page*10-10).limit(10).get({
			success: res => {
				if(res.data.length==0){
					return wx.showToast({
					  title: '没有更多记录了',
					  icon:'none'
					})
				}
				let list = this.data.list
				for(var i=0;i<res.data.length;i++){
					res.data[i].create_time = app.getDatetime(res.data[i].create_time)
				}
				list = list.concat(res.data)
				this.setData({
					list,
					page
				})
			}
		})
	},
	onReachBottom(){
		this.getData(this.data.page+1)
	}
})