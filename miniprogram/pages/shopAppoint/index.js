const app = getApp()
Page({

	data: {
		statusAppointTitle: app.globalData.statusAppointTitle,
		result: '页面加载中'
	},
	onLoad: function (options) {
		this.setData(options)
		this.onPullDownRefresh()
	},
	onPullDownRefresh() {
		app.cloud({
			name: 'shopAppoint',
			data: {
				_id: this.data._id
			},
			success: res => {
				wx.stopPullDownRefresh()
				res.data.appoint.create_time = app.getDatetime(new Date(res.data.appoint.create_time))
				if (res.data.appoint.use_time) res.data.appoint.use_time = app.getDatetime(new Date(res.data.appoint.use_time))
				if (res.data.appoint.cancel_time) res.data.appoint.cancel_time = app.getDatetime(new Date(res.data.appoint.cancel_time))
				this.setData(res.data)
				this.setData({
					result: ''
				})
			},
			fail: res => {
				this.setData({
					result: res.msg
				})
				wx.stopPullDownRefresh()
			}
		})
	},
	makeCall(){
		wx.makePhoneCall({
		  phoneNumber: this.data.appoint.phone,
		})
	},
	use(){
		app.cloud({
			name: 'shopAppoint',
			data: {
				_id: this.data._id,
				op:'use'
			},
			success: res => {
				wx.showToast({
				  title: res.msg,
				})
				res.data.appoint.create_time = app.getDatetime(new Date(res.data.appoint.create_time))
				if (res.data.appoint.use_time) res.data.appoint.use_time = app.getDatetime(new Date(res.data.appoint.use_time))
				if (res.data.appoint.cancel_time) res.data.appoint.cancel_time = app.getDatetime(new Date(res.data.appoint.cancel_time))
				this.setData(res.data)
				this.setData({
					result: ''
				})
			},
			loading:'提交中',
			def:true
		})
	},
	cancel(){
		app.cloud({
			name: 'shopAppoint',
			data: {
				_id: this.data._id,
				op:'cancel'
			},
			success: res => {
				wx.showToast({
					title: res.msg,
				  })
				res.data.appoint.create_time = app.getDatetime(new Date(res.data.appoint.create_time))
				if (res.data.appoint.use_time) res.data.appoint.use_time = app.getDatetime(new Date(res.data.appoint.use_time))
				if (res.data.appoint.cancel_time) res.data.appoint.cancel_time = app.getDatetime(new Date(res.data.appoint.cancel_time))
				this.setData(res.data)
				this.setData({
					result: ''
				})
			},
			loading:'提交中',
			def:true
		})
	}
})